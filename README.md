# Multiverse Swapper SDK

<img align="center" style="display:block; margin-x:auto;" src="https://gitlab.com/openbits1/multiverse-swapper-sdk/-/raw/master/public/multiverse-swapper-logo.svg" width="25%" alt="OpenBits logo" title="OpenBits Logo">
<br/>

- [Multiverse Swapper SDK](#multiverse-swapper-sdk)
  - [Description](#description)
  - [Installation and usage](#installation-and-usage)
  - [SDK functionalities](#sdk-functionalities)
    - [connectToProvider](#connecttoprovider)
      - [Parameters](#parameters)
    - [swapEtherToToken](#swapethertotoken)
      - [Parameters](#parameters-1)
    - [swapTokenToEther](#swaptokentoether)
      - [Parameters](#parameters-2)
    - [addSupportForERC20Token](#addsupportforerc20token)
      - [Parameters](#parameters-3)
  - [Live Example and Test](#live-example-and-test)
  - [Contribute](#contribute)
  - [License](#license)

## Description

A node package that makes very simple for developers to swap among ERC20 and other tokens. For swapping tokens, it uses in background the followings:

- Uniswap for swapping from ERC20 to ETH and from ETH to ERC20

By default, the swapper currently allows for the following swaps: 

- Ethereum to DAI (mainnet and rinkeby)
- DAI to Ethereum (mainnet and rinkeby)
- Ethereum to USDC (mainnet)
- USDC to Ethereum (mainnet)

However, you can very easily add support for other tokens by means of the [addSupportForERC20Token](#addsupportforerc20token) method. 

## Installation and usage

<img align="center" src="https://gitlab.com/cervoneluca/openbits/-/raw/master/assets/logo-black.png" height="50px" alt="OpenBits logo" title="OpenBits Logo"> This package is only served through OpenBits. 

To install OpenBits follow the instruction at <a href="https://openbits.world" target="_blank">openbits.world</a>

When OpenBits is installed on your machine, then install the Multiverse Swapper as followings: 

```
openbits install @openbits/multiverse-swapper
```

After having installed the package you can import it in your node applications as following: 

```javascript
import multiverseSwapper from '@openbits/multiverse-swapper';
```

## SDK functionalities

Currently the SDK supplies the following methods: 

- connectToProvider(provider)
- swapEtherToToken(etherAmount, tokenName)
- swapTokenToEther(tokenAmount, tokenName)
- addSupportForERC20Token(jsonTokenObject)

### connectToProvider
#### Parameters
 - __provider__ _object_, the provider object to which the swapper must be connected. 

This allows to connect the Multiverse Swapper SDK to a provider. The application that uses the swapper must take care of retrieving the wanted provider. For instance, supposing that you want that your users rely on metamask, the following code will allow to connect metamask to the swapper: 

  ```javascript
  ethereum.request({ method: 'eth_requestAccounts' })
  .then(() => {
    multiverseSwapper.connectToProvider(window.ethereum)
      .then(() => {
        console.log(multiverseSwapper.providerInfo);
      });
  })
  .catch((err) => {
    console.log(`You must approve metamask - ${err}`);
  });
  ```

### swapEtherToToken
#### Parameters
  - __etherAmount__ _string_, the amount of eth you want to swap
  - __tokenName__ _string_, the symbol of the token you want to receive back (i.e. DAI, USDC)


This allows to swap the given amount of ETH to the specified token. The amount of tokens that you will receive depends by the value the token has on the Uniswap exchanger when the swap is performed. The following is an example on how to swap 0.005 ETH to DAI. 

```
multiverseSwapper.swapEtherToToken('0.005', 'DAI');
```

If the users interacts with the swapper by means of metamask, it will ask to the user to confirm a transaction. 

The gas limit to pay is computed by means of the ethers.estimateGasLimit function, and it is doubled to be sure that the transaction never fails. 

The gas cost is currently hard coded to be 30e09. 

The duration limit of the transaction is currently hard coded to be 20 minutes. _NOTE that this does not mean that after 20 minutes the transaction will fail!_ If the swap is not actually performed after 20 minutes (for instance when the gas cost is very high), the transaction will be reverted after it is mined. 


### swapTokenToEther
#### Parameters
  - __tokenAmount__ _string_, the amount of token you want to swap
  - __tokenName__ _string_, the symbol of the token you want to receive back (i.e. DAI, USDC)

This allows to swap the given amount of the specified token to ETH. The amount of ETH that you will receive depends by the value the token has on the Uniswap exchanger when the swap is performed. The following is an example on how to swap 3.25 USDC to ETH (and it will work only on the mainnet since the USDC<->ETH pair does not exist on the rinkeby network). 

```
multiverseSwapper.swapTokenToEther('3.25', 'USDC');
```

If the users interacts with the swapper by means of metamask, it will ask to the user to confirm __two__ transactions. 

The first transaction is intended to approve the Uniswap router to spend the specified token on the behalf of the user. 

The second transaction is intended to actually swap the tokens to ETH. 

### addSupportForERC20Token
#### Parameters
  - __jsonTokenObject__ _object_, the json object that contains the info about the token for which you want to add support. It must be formatted in a specific way as following: 
```json
"TOKEN_SYMBOL": {
    "name": "THE TOKEN NAME",
    "symbol": "THE TOKEN SYMBOL",
    "addresses": { 
        "MAINNET": "THE ADDRESS OF THE TOKEN ON THE MAINNET",
        "1": "THE ADDRESS OF THE TOKEN ON THE MAINNET",
        "RINKEBY": "THE ADDRESS OF THE TOKEN ON THE RINKEBY TEST NETWORK",
        "4": "THE ADDRESS OF THE TOKEN ON THE RINKEBY TEST NETWORK",
    } 
}
```

This allows to programmatically add support for a new ERC20 token that is different from the default ones (DAI and USDC).

The addresses of the token must be duplicated for the network name and the network number. If you want that a token is not supported on a specific network, just do not add the address of that network. For an example on how to do not add support for a specific network, take a look to USDC configuration in the 'configs/tokens.json' file.

The following example shows how to enable the Multiverse Swapper to swap from UNI to ETH and vice versa:

```javascript
    const uniToken = {
      UNI: {
        name: 'UNI Coin',
        symbol: 'UNI',
        addresses: {
          MAINNET: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          RINKEBY: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          1: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          4: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
        },
      },
    };
    multiverseSwapper.addSupportForERC20Token(uniToken);
```

 __NOTE: The current version of the SDK supports only the tokens that have a direct pair on Uniswap__.

## Live Example and Test

To run a live example (with a very basic UI), do the followings: 

 - fork this project
 - run `npm install`
 - run `npm run dev`
 - open your browser on http://localhost:1234

After having done that, you can play around with the swapper. A notification mechanism is still not implemented, so the best way to see if your transactions where approved is to open the browser console and await, something will be printed soon or later 😁.

You can also modify files in the example folder and results will be reloaded automatically.

## Contribute

Download the code, develop, do a merge request. We will evaluate your code and we'll merge it if it makes sense.

## License 

This software was created and authored by the OpenBits Multiverse and it is released under the MIT license. 