import multiverseSwapper from '../src/index';

const onLoad = () => {
  const ethereumButton = document.querySelector('.enableEthereumButton');
  const addUNIToken = document.querySelector('.addUniToken');
  const swapButton = document.querySelector('.swapTokenToEthButton');
  const swapButtonUSDC = document.querySelector('.swapTokenUSDCToEthButton');
  const swapButtonUNI = document.querySelector('.swapTokenUNIToEthButton');
  const swapButtonETHDAI = document.querySelector('.swapEthToDAIButton');
  const swapButtonETHUSDC = document.querySelector('.swapEthToUSDCButton');
  const swapButtonETHUNI = document.querySelector('.swapEthToUNIButton');

  ethereumButton.addEventListener('click', () => {
    ethereum.request({ method: 'eth_requestAccounts' })
      .then(() => {
        multiverseSwapper.connectToProvider(window.ethereum)
          .then(() => {
            console.log(multiverseSwapper.providerInfo);
          });
      })
      .catch((err) => {
        console.log(`You must approve metamask - ${err}`);
      });
  });

  addUNIToken.addEventListener('click', () => {
    const uniToken = {
      UNI: {
        name: 'UNI Coin',
        symbol: 'UNI',
        addresses: {
          MAINNET: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          RINKEBY: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          1: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
          4: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
        },
      },
    };
    multiverseSwapper.addSupportForERC20Token(uniToken);
    console.log('Added support for UNI on mainnet and on rinkeby');
  });

  swapButton.addEventListener('click', () => {
    multiverseSwapper.swapTokenToEther('3.25', 'DAI')
      .then(console.log)
      .catch(console.log);
  });
  swapButtonUSDC.addEventListener('click', () => {
    multiverseSwapper.swapTokenToEther('3.25', 'USDC')
      .then(console.log)
      .catch(console.log);
  });
  swapButtonUNI.addEventListener('click', () => {
    multiverseSwapper.swapTokenToEther('1', 'UNI')
      .then(console.log)
      .catch(console.log);
  });
  swapButtonETHDAI.addEventListener('click', () => {
    multiverseSwapper.swapEtherToToken('0.005', 'DAI')
      .then(console.log)
      .catch(console.log);
  });
  swapButtonETHUSDC.addEventListener('click', () => {
    multiverseSwapper.swapEtherToToken('0.005', 'USDC')
      .then(console.log)
      .catch(console.log);
  });
  swapButtonETHUNI.addEventListener('click', () => {
    multiverseSwapper.swapEtherToToken('0.005', 'UNI')
      .then(console.log)
      .catch(console.log);
  });
};

onLoad();
