import {
  Fetcher,
} from '@uniswap/sdk';
import { ethers } from 'ethers';

const utils = {
  async fetchToken(tokenName, providerInfo, tokens) {
    console.log(tokens);
    const { chainId } = providerInfo.provider.network;

    // if the tokens is not supported throws TOKEN_NOT_SUPPORTED;
    if (!tokens[tokenName]) {
      throw new Error(`TOKEN_NOT_SUPPORTED-fetchToken: ${tokenName} tokens are not supported by the multiverse swapper`);
    }
    // if the tokens is not supported in the given network throws TOKEN_NOT_SUPPORTED_ON_NETWORK;
    if (!tokens[tokenName].addresses[chainId]) {
      throw new Error(`TOKEN_NOT_SUPPORTED_ON_NETWORK-fetchToken: ${tokenName} tokens are not supported on network ${chainId}`);
    }
    const tokenAddress = ethers.utils.getAddress(tokens[tokenName].addresses[chainId]);
    const tokenFullName = tokens[tokenName].name;

    // fetch the token data from the configuration and from the uniswap SDK
    const token = await Fetcher.fetchTokenData(
      chainId,
      tokenAddress,
      providerInfo.provider,
      tokenName,
      tokenFullName,
    );
    console.log(token);
    return token;
  },
};

export default utils;
