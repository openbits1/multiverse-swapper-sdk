/**
 * This module connects to the given provider
*/
import { ethers } from 'ethers';

/**
 * Connects to the given provider and returns the provider and information about
 * the current logged account
 * @param {Object} provider the provider to which connect
 */
const connectProvider = async (provider) => {
  const p = new ethers.providers.Web3Provider(provider);
  const address = provider.selectedAddress;
  const balanceWei = await p.getBalance(address);
  const balanceETH = ethers.utils.formatEther(balanceWei);
  return {
    provider: p,
    signer: p.getSigner(),
    address,
    balanceETH,
  };
};

export default connectProvider;
