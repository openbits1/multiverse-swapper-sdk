import {
  WETH,
  Fetcher,
  Trade,
  Route,
  TokenAmount,
  TradeType,
  Percent,
} from '@uniswap/sdk';
import { ethers } from 'ethers';
import utils from './utils';
import abis from '../configs/abis.json';

/**
 *
 * @param {*} ethAmount the ethereum amount to swap
 * @param {*} tokenName the token name which we want swap to
 * @param {*} providerInfo the provider info object
 * (must be created by means of the connectProvider method of this library)
 * @param {object} tokensList the object containing the list of supported tokens
 */
const swapEthToken = async (ethAmount, tokenName, providerInfo, tokensList) => {
  const { chainId } = providerInfo.provider.network;

  // fetch the token data
  const token = await utils.fetchToken(tokenName, providerInfo, tokensList);

  // fix the token amount as a string representing a number with the required number of decimals
  const fixedEthAmount = ethers.utils.parseEther(ethAmount);

  const pair = await Fetcher.fetchPairData(token, WETH[chainId], providerInfo.provider);
  const route = new Route([pair], WETH[chainId]);

  const trade = new Trade(
    route,
    new TokenAmount(WETH[chainId], fixedEthAmount.toString()),
    TradeType.EXACT_INPUT,
  );
  const slippageTolerance = new Percent('50', '10000');
  const amountOutMin = trade.minimumAmountOut(slippageTolerance);

  const amountOutMinBigNumber = ethers.utils.parseUnits(
    amountOutMin.toSignificant(token.decimals).toString(),
    token.decimals,
  );

  const inputAmountBigNumber = ethers.utils.parseEther(
    trade.inputAmount.toSignificant(18).toString(),
  );

  const path = [
    WETH[chainId].address,
    token.address,
  ];

  const to = providerInfo.address;
  const deadline = Math.floor(Date.now() / 1000) + 60 * 20;

  // prepare the actual transaction
  const uniswapAddress = abis.uniswap.addresses[chainId];
  const swapETHToTokenFunctionAbi = abis.uniswap.functions.swapExactETHForTokens;

  const uniswapContract = new ethers.Contract(
    uniswapAddress,
    swapETHToTokenFunctionAbi,
    providerInfo.provider,
  );
  const uniswapContractWithSigner = uniswapContract.connect(providerInfo.signer);

  const estimateGas = await uniswapContractWithSigner.estimateGas.swapExactETHForTokens(
    amountOutMinBigNumber.toHexString(),
    path,
    to,
    deadline,
    {
      value: inputAmountBigNumber.toHexString(),
    },
  );

  const tx = await uniswapContractWithSigner.swapExactETHForTokens(
    amountOutMinBigNumber.toHexString(),
    path,
    to,
    deadline,
    {
      gasPrice: 30e09,
      gasLimit: estimateGas.mul(ethers.BigNumber.from('2')),
      value: inputAmountBigNumber.toHexString(),
    },
  );
  const receipt = await tx.wait();
  return receipt;
};

export default swapEthToken;
