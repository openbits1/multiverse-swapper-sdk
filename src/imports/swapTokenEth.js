import {
  WETH,
  Fetcher,
  Trade,
  Route,
  TokenAmount,
  TradeType,
  Percent,
} from '@uniswap/sdk';
import { ethers } from 'ethers';
import utils from './utils';
import abis from '../configs/abis.json';

/**
 * Send a transaction to ethereum to swap from a given ERC20 amount to ETH
 * @param {number} tokenAmount the token amount to swap
 * @param {string} tokenName the token name to swap
 * @param {object} providerInfo an Object containing the info about the provider
 * @param {object} tokensList the object containing the list of supported tokens
 */
const swapTokenEth = async (tokenAmount, tokenName, providerInfo, tokensList) => {
  const { chainId } = providerInfo.provider.network;

  // fetch the token data
  const token = await utils.fetchToken(tokenName, providerInfo, tokensList);

  const fixedTokenAmount = ethers.utils.parseUnits(tokenAmount, token.decimals);
  const pair = await Fetcher.fetchPairData(WETH[chainId], token, providerInfo.provider);
  const route = new Route([pair], token);
  const trade = new Trade(route, new TokenAmount(token, fixedTokenAmount), TradeType.EXACT_INPUT);

  const slippageTolerance = new Percent('50', '10000');
  const amountOutMin = trade.minimumAmountOut(slippageTolerance);
  const amountOutMinBigNumber = ethers.utils.parseEther(
    amountOutMin.toSignificant(18).toString(),
  );

  const inputAmountBigNumber = ethers.utils.parseUnits(
    trade.inputAmount.toSignificant(token.decimals).toString(),
    token.decimals,
  );

  const path = [
    token.address,
    WETH[chainId].address,
  ];

  const to = providerInfo.address;
  const deadline = Math.floor(Date.now() / 1000) + 60 * 20;

  // prepare the actual transaction
  const uniswapAddress = abis.uniswap.addresses[chainId];
  const swapTokenToETHFunctionAbi = abis.uniswap.functions.swapExactTokensForETH;
  const approveFunctionAbi = abis.uniswap.functions.approve;

  // approve the router contract to spend our tokens
  const tokenContract = new ethers.Contract(
    token.address,
    approveFunctionAbi,
    providerInfo.provider,
  );
  const tokenContractWithSigner = tokenContract.connect(providerInfo.signer);
  const txApprove = await tokenContractWithSigner.approve(
    uniswapAddress, inputAmountBigNumber.toHexString(),
  );

  await txApprove.wait();

  const uniswapContract = new ethers.Contract(
    uniswapAddress,
    swapTokenToETHFunctionAbi,
    providerInfo.provider,
  );
  const uniswapContractWithSigner = uniswapContract.connect(providerInfo.signer);

  // create the actual transaction
  const estimateGas = await uniswapContractWithSigner.estimateGas.swapExactTokensForETH(
    inputAmountBigNumber.toHexString(),
    amountOutMinBigNumber.toHexString(),
    path,
    to,
    deadline,
  );

  const tx = await uniswapContractWithSigner.swapExactTokensForETH(
    inputAmountBigNumber.toHexString(),
    amountOutMinBigNumber.toHexString(),
    path,
    to,
    deadline,
    {
      gasPrice: 25e09,
      gasLimit: estimateGas.mul(ethers.BigNumber.from('2')),
    },
  );

  const receipt = await tx.wait();
  return receipt;
};

export default swapTokenEth;
