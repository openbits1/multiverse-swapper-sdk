import connectProvider from './imports/connectProvider';
import swapTokenEth from './imports/swapTokenEth';
import swapEthToken from './imports/swapEthToken';
import tokens from './configs/tokens.json';

const multiverseSwapper = {
  /**
   * This will contain all the info related to the Web3 provider, more specifically:
   * - provider: the actual provider object
   * - signer: the signer object detected by the given provider
   * - address: the address used by the current provider (i.e. the user address)
   * - balanceETH: the ETH balance of the current used address
   */
  providerInfo: null,

  /**
   * This will contain all the supported tokens. Initially tokens list is the default one
   * imported from the 'configs/token.json' file. Support for other tokens can be added
   * by means of the addERC20Token method.
   */
  tokensList: tokens,

  /**
   * Connect the ethereum provider to ethers.js. After having done that,
   * fills the provider object with the info about the current used address
   * @param {object} provider the provider to which connect (i.e. metamask, wallet-connect)
   * @return {void}
   */
  async connectToProvider(provider) {
    try {
      this.providerInfo = await connectProvider(provider);
    } catch (err) {
      throw new Error(`UNKNOWN_ERROR-connectToProvider: Something went wrong in retrieving your Ethereum provider - Err: ${err}`);
    }
  },
  /**
   * Swaps the specified amount of token of the specified type into Ethers
   * @throws {error} NoProvider if a provider was not connected to
   * the swapper before calling this method
   * @param {string} tokenAmount the amount of tokens that must be swapped
   * @param {string} tokenName the name of the tokens that must be swapped
   * @return {object} the receipt of the transaction
   */
  async swapTokenToEther(tokenAmount, tokenName) {
    if (!this.providerInfo || !this.providerInfo.provider) {
      throw new Error('NO_PROVIDER-swapTokenToEther: you must connect a provider with the "connectToProvider" function before calling this');
    }
    // check if parameters are of the correct type
    if (typeof tokenAmount !== 'string') {
      throw new Error('PARAMETER_MUST_BE_A_STRING-swapTokenToEther: the tokenAmount parameter must be a string');
    }
    if (typeof tokenName !== 'string') {
      throw new Error('PARAMETER_MUST_BE_A_STRING-swapTokenToEther: the tokenName parameter must be a string');
    }
    const receipt = await swapTokenEth(tokenAmount, tokenName, this.providerInfo, this.tokensList);

    return receipt;
  },
  /**
   * Swaps the specified amount of Ethereum to the most convenient amount of tokens
   * of the specified type
   * @throws {error} NO_PROVIDER if a provider was not connected to
   * the swapper before calling this method
   * @param {string} etherAmount he amount of Ethereum that must be swapped
   * @param {string} tokenName the name of the tokens to which the ethereum must be swapped
   * @return {object} the receipt of the transaction
  */
  async swapEtherToToken(etherAmount, tokenName) {
    if (!this.providerInfo || !this.providerInfo.provider) {
      throw new Error('NO_PROVIDER-swapEtherToToken: you must connect a provider with the "connectToProvider" function before calling this');
    }
    // check if parameters are of the correct type
    if (typeof etherAmount !== 'string') {
      throw new Error('PARAMETER_MUST_BE_A_STRING-swapEtherToToken: the etherAmount parameter must be a string');
    }
    if (typeof tokenName !== 'string') {
      throw new Error('PARAMETER_MUST_BE_A_STRING-swapEtherToToken: the tokenName parameter must be a string');
    }
    const receipt = await swapEthToken(etherAmount, tokenName, this.providerInfo, this.tokensList);
    return receipt;
  },
  /**
   * Programmatically add support to the SDK for a new token
   * @param {object} jsonTokenObject the json token object, see the below example.
   * @example
   * UNI: {
   *     name: 'UNI Coin',
   *     symbol: 'UNI',
   *     addresses: {
   *       MAINNET: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
   *       RINKEBY: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
   *       1: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
   *       4: '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984',
   *     },
   *   },
   * @return {void}
  */
  addSupportForERC20Token(jsonTokenObject) {
    // check if parameters are of the correct type
    if (typeof jsonTokenObject !== 'object') {
      throw new Error('PARAMETER_MUST_BE_AN_OBJECT-addSupportForERC20Token: the jsonTokenObject parameter must be an object');
    }
    this.tokensList = {
      ...this.tokensList,
      ...jsonTokenObject,
    };
  },
};

export default multiverseSwapper;
